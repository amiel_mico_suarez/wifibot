(function(){'use strict'; module.exports = function(NODE) {return function(params){
	
	var ITEM = this;
	
	var model = NODE.MODELS[params.schema].obj;

	var prop_keys = Object.keys(model._prop);
	
	var base_item = {};
	if (ITEM.isNew == false){
		base_item = JSON.parse(JSON.stringify(ITEM));
		ITEM._prop = JSON.parse(JSON.stringify(ITEM._prop_update));
	}
	
	var error = [];
	var obj = {
		_prop: {}
	}
	
	var validate_index = 0;
	for (var i = 0; i < prop_keys.length; ++i) {validateKey(prop_keys[i])};

	function validateKey(key){
		
		var key_params = model._prop[key];
		var key_value = ITEM._prop[key];

		if ((ITEM._force != true && key_value != undefined) || ITEM._force == true){
			var is_foreign = Boolean(key_params.ref != undefined && key_params.custom != undefined && key_params.custom.foreign == true)
			var is_foreign_external = Boolean(key_params.custom != undefined && key_params.custom.foreign_external == true && key_params.custom.foreign_ref)
			var is_blank = Boolean(
				key_value == undefined || 
				key_value == null ||
				key_value === "" || 
				key_value.equals([]) 
			)
			

			var skip_validation = false;
		
			if (ITEM.isNew == false){
				switch(key_params.type){
					case String:
					case Number:
					case Date:
					case Boolean:
						if (key_value == base_item._prop[key]){skip_validation = true;}
						break;

					case Array:
						if (base_item._prop[key].equals(key_value)){skip_validation = true;}
						break

					case Object:
						skip_validation = true;
						break
				}		
				
			}

			// if (key_params.custom != undefined && key_params.custom.auto_value != undefined){
			// 	skip_validation = true
			// }

			var is_soft_delete = false;

			if (ITEM.isNew != true){
				if (key_params.custom != undefined && key_params.custom.soft_delete != undefined && key_params.custom.soft_delete.constructor === Array && key_params.custom.soft_delete.length > 0){
					for (var i = 0; i < key_params.custom.soft_delete.length; ++i) {
						var p = key_params.custom.soft_delete[i];
						if (p == key_value){is_soft_delete = true; break}
					}
				}
			}

			if (is_soft_delete == true){
				errorSoftDelete(key);
			} else {
				
				if (skip_validation == true){
					if (ITEM._force == true){
						passValidation(key, key_value);
					} else {
						finishValidation()
					}
				} else {
					if(
						key_params.required && is_blank
					){
						errorRequired(key)
					} else {
						if (!NODE.LO.isEmptyArray(key_params.required_with)){
							var blank_array = [is_blank];

							for (var i = 0; i < key_params.required_with.length; ++i) {
								var p = key_params.required_with[i];
								var with_value = NODE.HELPERS.keyScrapper.get(ITEM, p)

								var with_blank = Boolean(
									with_value == undefined || 
									with_value == null ||
									with_value === "" || 
									with_value.equals([])
								)

								blank_array.push(with_blank)
							}

							var required_with_pass = false;
							for (var i = 0; i < blank_array.length; ++i) {
								var p = blank_array[i];
								if (p == false){required_with_pass = true}
							}

							if (required_with_pass == false){errorRequiredWith(key, key_params.required_with)}
						}

						

						
						if (!is_blank){
							var pass_value = key_value;

						
								
							if ((key_params.unique == true) || (
								key_params.custom != undefined && 
								!NODE.LO.isEmptyArray(key_params.custom.unique_with)
							)){
								var unique_filter = {};

								if (key_params.unique == true){
									unique_filter["_prop."+key] = key_value
								} else {
									unique_filter["$and"] = [];
									var unique_me = {};
									unique_me["_prop."+key] = key_value;
									unique_filter["$and"].push(unique_me);

									for (var i = 0; i < key_params.custom.unique_with.length; ++i) {
										var p = key_params.custom.unique_with[i];
										var unique_key = p.substr(6, p.length - 1);
										var unique_obj = {};
										unique_obj[p] = ITEM._prop[unique_key];
										unique_filter["$and"].push(unique_obj);
									}
								}

								
								
								NODE.COLS[params.schema].count(unique_filter, function(err, count){
									if (count > 0){errorUnique(key)}
									else {validateType()};
								})
							} else {
								validateType();
							}

							function validateForeign(key, foreign_main_filter){
								
								if (is_foreign == true){
									if (!NODE.LO.isEmptyString(key_value)){
										if (key_params.custom != undefined && key_params.custom.filter != undefined){
											
											var foreign_result_ids = [];
											var constraint_keys = Object.keys(key_params.custom.filter);
											var constraint_data = {};
											
											for (var i = 0; i < constraint_keys.length; ++i) {
												var p = constraint_keys[i];
												if (typeof key_params.custom.filter[p] !== "object"){
													contraintDataCreate(key_params.ref, "_id", p)
												} else  {
													key_params.custom.filter[p]
													contraintDataCreate(key_params.custom.filter[p]["ref"], key_params.custom.filter[p]["filter_key"], p)
												}
											}

											function contraintDataCreate(key, filter_key, value){
												if (constraint_data[key] == undefined){
													constraint_data[key] = {
														projection : "_id ",
														filter_key: filter_key
													}
												}

												constraint_data[key].projection += value + " "
											}

											if ( constraint_data[key_params.ref] == undefined){constraint_data[key_params.ref].projection = "_id"}

											
											NODE.COLS[key_params.ref].find({"_id": {"$in": foreign_main_filter}}, constraint_data[key_params.ref].projection+' _prop.status', function(err, foreign_result){
												if (foreign_result != undefined && foreign_result != null && foreign_result.length == foreign_main_filter.length){
													var constraint_pass = true
													for (var i = 0; i < foreign_result.length; ++i) {
														var p = foreign_result[i]
														
														if(p._prop.status){if(p._prop.status != 'Active'){errorInactiveForeign(key)}};
													
														foreign_result_ids.push(p._id);
														for (var i2 = 0; i2 < constraint_keys.length; ++i2) {
															var p2 = constraint_keys[i2];
															var filter_value = key_params.custom.filter[p2];
														
															if (typeof key_params.custom.filter[p2] !== "object"){
																var data_compare = filter_value;
																
																if (typeof key_params.custom.filter[p2] === "string"){
															
																	if (filter_value.includes("$dependent-")){
																		var constraint_key = filter_value.replace("$dependent-","")
																		data_compare = ITEM._prop[constraint_key]
																	}
																}

																var p_keys = p2.split(".");
																var p_compare = p;
																var p_key_index = 0;
																while(p_key_index < p_keys.length){p_compare = p_compare[p_keys[p_key_index]]; p_key_index++};
																

																var compare_type = key_params.type;
																if (constraint_key != undefined){compare_type = model._prop[constraint_key].type}

																switch(compare_type){
																	case String:
																	case Number:
																		if (
																			constraint_key != undefined &&
																			model._prop[constraint_key] != undefined &&
																			model._prop[constraint_key].custom != undefined && 
																			model._prop[constraint_key].custom.child_type != undefined &&
																			model._prop[constraint_key].custom.object_ref != undefined
																		){
																			if (p_compare != data_compare[model._prop[constraint_key].custom.object_ref]){constraint_pass = false}
																		} else {
																			if (p_compare != data_compare){constraint_pass = false}
																		}
																		break;
																	
																	case Array:

																		for (var i3 = 0; i3 < data_compare.length; ++i3) {
																			if (
																				model._prop[constraint_key].custom != undefined && 
																				model._prop[constraint_key].custom.child_type != undefined &&
																				model._prop[constraint_key].custom.object_ref != undefined
																			){
																				var p3 = data_compare[i3][model._prop[constraint_key].custom.object_ref];
																			} else {
																				var p3 = data_compare[i3];
																			}

																			var constraint_found = false;

																			for (var i4 = 0; i4 < p_compare.length; ++i4) {
																				var p4 = p_compare[i4];
																				if (p3 == p4){constraint_found = true; break}
																			}

																			if (constraint_found == false){constraint_pass = false; break}
																		}

																		break
																}
															} else {
																// CUSTOM UNFINISHED
															}
														}
													}
													
													if (constraint_pass){
														passValidation(key, pass_value);
													} else {
														errorInvalidConstraint(key, constraint_keys);
													}
												} else {
													errorInvalidType(key, "foreign key of " + key_params.ref)
												}
											})
										} else {
											NODE.COLS[key_params.ref].count({"_id": {"$in": foreign_main_filter}}, function(err, count){
												if (count > 0){
													passValidation(key, pass_value);
												} else {
													errorInvalidType(key, "foreign key of " + key_params.ref)
												}
											})
										}
									} else {
										passValidation(key, pass_value)
									}
								} else if (is_foreign_external == true){
									var count_filter = {}; 
									var query_check = true;
									if (key_params.custom != undefined && key_params.custom.foreign_collection != undefined){
										var foreign_filter_value = "_id";
										if (key_params.custom.foreign_filter != undefined){foreign_filter_value = key_params.custom.foreign_filter}
										
										if (foreign_filter_value == "_id" ){
											var object_id_filter = [];
											for (var i = 0; i < foreign_main_filter.length; ++i) {
												var p = foreign_main_filter[i];
												object_id_filter.push(p)
												object_id_filter.push(NODE.HELPERS.validateBasic.objectId(p))
											}
											count_filter[foreign_filter_value] = {"$in": object_id_filter};
										} else {
											count_filter[foreign_filter_value] = {"$in": foreign_main_filter}
										}
										
									} else {
										query_check = false;
										errorBrokenExternalForeign(key)
									}

									if (query_check == true){
										
										NODE.HELPERS.http.request({
											route: key_params.custom.route,
											action: "count",
											schema: key_params.custom.foreign_collection.toLowerCase(),
											server: key_params.custom.server,
											filter: count_filter,
										}, function(error, total_count){
											if (total_count != undefined && total_count != null && total_count > 0){
												passValidation(key, pass_value);
											} else {
												errorInvalidType(key, "foreign key of " + key_params.custom.server + " - " + key_params.custom.external_ref.toLowerCase())
											}
										})
									}
								}
							}
							
							function validateType(){
								if (key_params.custom != undefined && key_params.custom.values != undefined && key_params.custom.values.constructor === Array){
									if (key_params.type === Array){
										pass_value = null;

										if (key_value && key_value.constructor === Array){
											var pass_array = [];

											for (var i = 0; i <  key_params.custom.values.length; ++i) {
												var p = key_params.custom.values[i]._id;
												for (var i2 = 0; i2 < key_value.length; ++i2) {
													var p2 = key_value[i2];
													if (NODE.LO.isString(p)){
														if (String(p) === String(p2)){ pass_array.push(p); break }
													} else if (NODE.LO.isObject(p)) {
														if (String(p._id) === String(p2)){ pass_array.push(p); break }
													}
												}
											}

											if (pass_array.length > 0){pass_value = pass_array}
											if(pass_value == null && !key_params.required){pass_value = []}
										}
									} else {
										pass_value = null;
										
										for (var i = 0; i <  key_params.custom.values.length; ++i) {
											var p = key_params.custom.values[i];
											
											if (NODE.LO.isObject(p)) {
												var key_string = p._id;
												if (NODE.LO.isNil(key_string) && !NODE.LO.isNil(p.value)){key_string = p.value}

												if (String(key_string) === String(key_value)){ pass_value = p; break }
											} else {
												if (String(p) === String(key_value)){ pass_value = p; break }
											}
										}
									}
									if (pass_value != null){
										passValidation(key, pass_value)
									} else {
										errorInvalidSelection(key, key_params.custom.values)
									}
								} else {
									switch(key_params.type){
										case String:
											pass_value = String(key_value);
											
											if (is_foreign == true || is_foreign_external == true){
												validateForeign(key, [pass_value]);
											} else {
												passValidation(key, pass_value)
											}

											break

										case Boolean:
											pass_value = Boolean(key_value);
											if (Boolean(key_value) == true || Boolean(key_value) == false ){
												passValidation(key, pass_value);
											} else {
												errorInvalidType(key, "boolean");
											}

											break

										case Number:
											pass_value = Number(key_value);
											if (!isNaN(Number(key_value))){
												passValidation(key, pass_value);
											} else {
												errorInvalidType(key, "number");
											}
											break

										case Date:
											pass_value = NODE.SDATE.get(key_value);
											if (NODE.SDATE.get(key_value) != "Invalid Date"){
												passValidation(key, pass_value);
											} else {
												errorInvalidType(key, "date");
											}
											break

										case Object:
											passValidation(key, pass_value);
											break
										
										case Array:
											if (key_value && key_value.constructor === Array){
												var foreign_check = [];

												if (key_params.custom != undefined && key_params.custom.child_type != undefined){
													var child_type_keys = Object.keys(key_params.custom.child_type);

													for (var i = 0; i < key_value.length; ++i) {
														var p = key_value[i];

														for (var i2 = 0; i2 < child_type_keys.length; ++i2) {
															var p2 = child_type_keys[i2];
															switch(key_params.custom.child_type[p2]){
																case String:
																	key_value[i][p2] = String(p[p2]);
																	break
																case Number:
																	if (!isNaN(Number(p[p2]))){
																		key_value[i][p2] = Number(p[p2])
																	} else {
																		errorInvalidType(key + "." + p2, "number", true);
																	}
																	break
																case Date:
																	if ( NODE.SDATE.get(p[p2])){
																		key_value[i][p2] = NODE.SDATE.get(p[p2])
																	} else {
																		errorInvalidType(key + "." + p2, "date", true);
																	}
																	break
																case Boolean:
																	if (Boolean(p[p2]) == true || Boolean(p[p2]) == false ){
																		key_value[i][p2] = Boolean(p[p2])
																	} else {
																		errorInvalidType(key + "." + p2, "boolean", true);
																	}
																	break
															}
															
														}
													}
												}
												
												if (is_foreign == true || is_foreign_external == true){
													if (key_params.custom != undefined && key_params.custom.object_ref != undefined){
														foreign_check = [];
														for (var i = 0; i < key_value.length; ++i) {
															var p = key_value[i];
															var key_value_unique = true;
															for (var i2 = 0; i2 < foreign_check.length; ++i2) {
																var p2 = foreign_check[i2];
																if (p2 == p[key_params.custom.object_ref]){key_value_unique = false; break}
															}

															if (key_value_unique){foreign_check.push(p[key_params.custom.object_ref])}
														}
													} else {
														for (var i = 0; i < key_value.length; ++i) {
															var p = key_value[i];
															var key_value_unique = true;
															for (var i2 = 0; i2 < foreign_check.length; ++i2) {
																var p2 = foreign_check[i2];
																if (p2 == p){key_value_unique = false; break}
															}

															if (key_value_unique){foreign_check.push(p)}
														}
													}

													if (foreign_check.length > 0){
														validateForeign(key, foreign_check);
													} else {
														passValidation(key, key_value);
													}
												} else {
													passValidation(key, pass_value);
												}
											} else {
												errorInvalidType(key, "array");
											}
											break

										
									}
								}
							}
						} else {
							passValidation(key, key_value);
						}
					}
				}
			}

			
			
		} else {
			finishValidation();
		}
	}

	function passValidation(key, value){
		obj._prop[key] = value;
		finishValidation();
	}

	function errorRequiredWith( key, required_with_list){
		var required_with_string = key.toUpperCase();
		for (var i = 0; i < required_with_list.length; ++i) {
			var p = required_with_list[i];
			var comma = ", "
			if (i == required_with_list.length - 1){comma = " or "}
			required_with_string += comma + p.replace("_prop.","").toUpperCase();
		}

		error.push({type: "basic", message: required_with_string +" are required", key:  "_prop."+ key});
		finishValidation();
	}

	function errorRequired(key){
		error.push({type: "basic", message: key.toUpperCase() +" is required", key:  "_prop."+ key});
		finishValidation();
	}

	function errorUnique(key){
		error.push({type: "basic", message: key.toUpperCase()+" needs to be unique", key:  "_prop."+ key});
		finishValidation();
	}

	function errorInvalidType(key, type, stop){
		error.push({type: "basic", message: key.toUpperCase()+" needs to be a/an " + type, key:  "_prop."+ key});
		if (stop != true){finishValidation()}
	}

	function errorInvalidSelection(key, selection, stop){
		var select_string = "";

		for (var i = 0; i < selection.length; ++i) {
			var p = selection[i];
			if (i == selection.length - 1){
				select_string += " or " + p 
			} else {
				select_string += ", " + p 
			}
		}	
		
	

		var subtitle_string = select_string.lastChars(select_string.length - 2);

		error.push({type: "basic", message: key.toUpperCase()+" needs to be either " + subtitle_string, key: "_prop."+ key});
		if (stop != true){finishValidation()}
	}

	function errorSoftDelete(key){
		error.push({type: "not allowed", message: key.toUpperCase()+" cannot be edited via the UPDATE action ", key: "_prop."+ key});
		finishValidation()
	}

	function errorInvalidConstraint(key, constraint, stop){
		error.push({type: "constraint failure", message: key.toUpperCase()+" fails the constraint set by " + constraint, key: "_prop."+ key});
		if (stop != true){finishValidation()}
	}

	function errorInactiveForeign(key){
		error.push({type: "basic", message: "inactive "+key.toUpperCase()+" detected", key: "_prop."+ key});
		finishValidation();
	}

	function errorBrokenExternalForeign(key){
		error.push({type: "broken model", message: key.toUpperCase()+" does not have a foreign filter ", key: "_prop."+ key});
		finishValidation();
	}

	function finishValidation(){	
		
		validate_index++;
		if (validate_index == prop_keys.length){
			params.callback(error)
		}
	}
	
}}})()