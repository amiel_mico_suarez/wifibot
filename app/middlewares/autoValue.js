(function(){'use strict'; module.exports = function(NODE) {return function(params){
	var ITEM = this;
	

	var model = NODE.MODELS[params.schema].obj;
	var prop_keys = Object.keys(model._prop);

	var injectFunction = {
		"foreign_copy": function (key){
			var key_params = model._prop[key];
	
			if (
				key_params.custom.auto_value.type == "foreign_copy" &&
				key_params.custom.auto_value.foreign_copy_source != undefined &&
				key_params.custom.auto_value.foreign_copy_display != undefined
			){
				var parent_data = NODE.HELPERS.keyScrapper.get(model, key_params.custom.auto_value.foreign_copy_source);
				var parent_value = NODE.HELPERS.keyScrapper.get(ITEM, key_params.custom.auto_value.foreign_copy_source);
				var parent_filter = {"$or": [
					{"_id": parent_value},
					{"_id": NODE.HELPERS.objectId(parent_value)}
				]}
	
				if(parent_data.custom != undefined && (parent_data.custom.foreign == true || parent_data.custom.foreign_external == true)){
					if (parent_data.custom.foreign == true){
						NODE.COLS[parent_data.ref].find(parent_filter, function(err, items){
							if (items != undefined && items != null && items[0] != undefined){
								ITEM._prop[key] = NODE.HELPERS.keyScrapper.get(items[0], key_params.custom.auto_value.foreign_copy_display);
							};
							finishInjection()
						})
					} else if (parent_data.custom.foreign_external == true) {
						NODE.HELPERS.http.request({
							route: parent_data.custom.route,
							action: parent_data.custom.action,
							schema: parent_data.custom.foreign_collection.toLowerCase(),
							server: parent_data.custom.server,
							filter: parent_filter,
						}, function(error, items){
							if (items != undefined && items != null && items[0] != undefined){
								ITEM._prop[key] = NODE.HELPERS.keyScrapper.get(items[0], key_params.custom.auto_value.foreign_copy_display);
							};
							finishInjection()
						})
					}
				} else {
					finishInjection()
				}
			} else {
				finishInjection()
			}
		},

		"increment": function (key){
			if (ITEM.isNew == false){
				NODE.COLS[params.schema].findById(ITEM._id, function(err, original){
	
					var total = 0;
					var largest = 0;
					var smallest = 1000;
					var increment = 0;
					var default_increment = 0;
					
					if(model._prop[key].custom.auto_value.default_increment){default_increment = model._prop[key].custom.auto_value.default_increment}
					for(var i=0;i<prop_keys.length;++i){	
	
						var changed = false;
						if(typeof ITEM._prop[prop_keys[i]] != "object" && ITEM._prop[prop_keys[i]] != original._prop[prop_keys[i]]){
							changed = true;
						}else{
							if (ITEM._prop[prop_keys[i]] != undefined){
								changed = !ITEM._prop[prop_keys[i]].equals(original._prop[prop_keys[i]]);
							}
						}
	
						if(changed){
							if(model._prop[key].custom.auto_value != undefined && model._prop[key].custom.auto_value.increment_obj != undefined && model._prop[key].custom.auto_value.increment_obj[prop_keys[i]] != undefined){
								var custom_increment = model._prop[key].custom.auto_value.increment_obj[prop_keys[i]];
							}else{
								var custom_increment = default_increment;
							}
								total += custom_increment;
								if(largest < custom_increment){
									largest = custom_increment;
								}
								if(smallest > custom_increment){
									smallest = custom_increment;
								}
						}
					}
	
					if(model._prop[key].custom.auto_value && model._prop[key].custom.auto_value.increment_type){
						switch(model._prop[key].custom.auto_value.increment_type){
							case 'largest':
								increment = largest;
							break;
							case 'smallest':
								increment = smallest;
							break;
							case 'total':
								increment = total;
							break;
							case 'default':
								increment = default_increment;
							break;
						}
					}

					if(ITEM._prop[key] == original._prop[key]){
						ITEM._prop[key] += Number(Number(increment).toFixed(1));
					}
					finishInjection()
				})
			} else {

				if (NODE.LO.isNil(ITEM._prop[key])){
					for(var i=0;i<prop_keys.length;++i){
						if(
							model._prop[prop_keys[i]].custom &&
							model._prop[prop_keys[i]].custom.auto_value && 
							model._prop[prop_keys[i]].custom.auto_value.type == "increment"					
						){
							ITEM._prop[prop_keys[i]] = !NODE.LO.isNil(model._prop[prop_keys[i]].custom.auto_value.base) ? model._prop[prop_keys[i]].custom.auto_value.base : 0;
						}
					}
				}
				
				finishInjection()
			}
		},

		"pattern": function (key){
			if(ITEM.isNew == true){
				if(model._prop[key].custom.auto_value.base){
					var base = model._prop[key].custom.auto_value.base;
					if(base.includes("$test$")){
						base = base.replace("$test$", "000");
					}
					else if(base.includes("$_sequence$")){
						base = base.replace("$_sequence$", "1111");
					}
					ITEM._prop[key] = base;
					finishInjection()
				}else{
					finishInjection()
				}
			} else {
				finishInjection()
			}
		},

		"basic": function (key){
	
			var key_params = model._prop[key];
			var value = key_params.custom.auto_value.value;
			var is_foreign = Boolean(key_params.custom.foreign == true || key_params.custom.foreign_external == true);
			if(ITEM.isNew == true && value != undefined && value != null && (ITEM._prop[key] == undefined || ITEM._prop[key] == null)){
				
				if (key_params.custom.child_type != undefined){
					
					var child_type_keys = Object(key_params.custom.child_type);
	
					if (key_params.type == Array){
						var set_array = [];
						var set_array_index = 0;
						if (value.length > 0){
							for (var i = 0; i < value.length; ++i) {
								var p = value[i];
								setChildObject(p, function(child_value){
									if (child_value != null){set_array.push(child_value)};
									set_array_index++; 
									if (set_array_index == value.length){
										ITEM._prop[key] = set_array;
										finishInjection()
									}
								});
							}
						} else {
							ITEM._prop[key] = set_array;
							finishInjection()
						}
					} else {
						setChildObject(base_value, function(child_value){
							ITEM._prop[key] = child_value;
							finishInjection()
						})
					}
	
					if (!is_foreign){finishInjection()}
				} else {
					if (is_foreign){
						setForeignValue(value, function(foreign_value){
							ITEM._prop[key] = foreign_value;
							finishInjection();
						})
					} else {
						
						ITEM._prop[key] = setValue(key_params.type, value);
						finishInjection()
					}
				}
			} else {
				finishInjection();
			}
	
			function setForeignValue(filter_value, callback){
				if (key_params.custom.foreign == true){
					NODE.COLS[key_params.ref].find(filter_value, "_id", function(err, result){finishForeignQuery(result)})
				} else {
					NODE.HELPERS.http.request({
						route: key_params.custom.route,
						action: key_params.custom.action,
						schema: key_params.custom.foreign_collection.toLowerCase(),
						server: key_params.custom.server,
						filter: value,
					}, function(error, result){finishForeignQuery(result)})
				}
	
				function finishForeignQuery(foreign_results){
					if (key_params.type === Array){
						var foreign_array = [];
						for (var i = 0; i < foreign_results.length; ++i) {
							var p = foreign_results[i];
							foreign_array.push(String(p._id))
						}
	
						callback(foreign_array);
					} else {
						callback(setValue(key_params.type, foreign_results[0]._id));
					}
				}
			}
	
			function setChildObject(base_value, callback){
				var return_obj = {};
				if (child_type_keys != undefined){
					if (typeof base_value === "object"){
						if (child_type_keys.length > 0){
							var child_type_index = 0;
							for (var i = 0; i < child_type_keys.length; ++i) {
								var p = child_type_keys[i];
	
								if (is_foreign && key_params.custom.object_ref == p){
									setForeignValue(base_value[p], function(foreign_value){
										return_obj[p] = setValue(String, foreign_value);
										finishInjection();
									})
								} else {
									return_obj[p] = setValue(key_params.custom.child_type[p], base_value[p]);
									finishChildObject()
								}
							}
						} else {
							callback(null)
						}
					}
				} else {
					callback(null)
				}
	
				function finishChildObject(){
					child_type_index++
					if (child_type_index == child_type_keys.length){
						if (Object.keys(return_obj).length == 0){return_obj = null}
						callback(return_obj);
					}
				}
			}
			
			function setValue(type, base_value){
				
				var return_value = null;
				switch(type){
					case Boolean:
						return_value = Boolean(base_value);
						break

					case String:
						return_value = String(base_value);
						break
	
					case Number:
						if (!isNaN(Number(base_value))){return_value = Number(base_value)}
						break
	
					case Date:
						if (NODE.SDATE.get(base_value) != "Invalid Date"){return_value = NODE.SDATE.get(base_value)};
						break
	
					case Array:
						if (base_value.constructor === Array){return_value = base_value}
						break
				}


				
				return return_value;
			}
		},

		"session_id": function(key){
			if(ITEM.isNew == true){
				var check_children = NODE.LO.checkChildren(ITEM, "_dump.user._prop.helium_id");

				if (check_children){
					ITEM._prop[key] = ITEM._dump.user._prop.helium_id;
					finishInjection();
				} else {
					finishInjection()
				}
			} else {
				finishInjection();
			}
		},

		"custom": finishInjection,
	}

	var inject_index = 0;
	for (var i = 0; i < prop_keys.length; ++i) {readKey(prop_keys[i])};

	function readKey(key){
		var key_params = model._prop[key];

		if (key_params.custom != undefined && key_params.custom.auto_value != undefined && key_params.custom.auto_value.type != undefined){
			injectFunction[key_params.custom.auto_value.type](key);
		} else {
			finishInjection()
		}
	}

	function finishInjection(){
		inject_index++;
		
		if (inject_index >= prop_keys.length){
			
			params.callback()
		}
	}
	
}}})()