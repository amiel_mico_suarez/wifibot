(function(){'use strict'; module.exports = function(NODE) {return function(params){
	var ITEM = this;
	
	if (ITEM.isNew == false){
		if (ITEM._dump == undefined){ITEM._dump = {}};
		if (ITEM._dump.user == undefined){ITEM._dump.user = {"_prop": {}}};
		ITEM._prop_update = JSON.parse(JSON.stringify(ITEM._prop));


		
		var model_keys = Object.keys(NODE.MODELS[params.schema].obj._prop);
		model_keys.forEach((model_key)=>{
			if (
				NODE.MODELS[params.schema].obj._prop[model_key].type == Array &&
				!NODE.LO.isEmptyArray(ITEM._prop_update[model_key]) && 
				(
					NODE.LO.isNil(NODE.MODELS[params.schema].obj._prop[model_key].custom) ||
					(
						!NODE.LO.isNil(NODE.MODELS[params.schema].obj._prop[model_key].custom) &&
						NODE.MODELS[params.schema].obj._prop[model_key].custom.allow_duplicate_array_item != true
					)
				)
			){
				var unique_list = []
				ITEM._prop_update[model_key].forEach((model_value)=>{
					if (unique_list.length > 0){
						var unique = true;
						unique_list.forEach((unique_value)=>{
							if (NODE.LO.isEqual(model_value, unique_value)){unique = false}
						})
						if (unique){
							unique_list.push(model_value)
						}
						
					} else {
						unique_list.push(model_value)
					}
				})

				ITEM._prop_update[model_key] = unique_list;
			}
		})

		NODE.COLS[params.schema].findById(ITEM._id, function(err, original){
		
			ITEM._prop = JSON.parse(JSON.stringify(original._prop));

			if (ITEM._history == undefined){ITEM._history = []};
			var is_forced = false;
			if (ITEM._force == true){is_forced = ITEM._force}
			var history_push = {by: ITEM._dump.user._prop, from: {}, to: {}, forced: is_forced, date: NODE.SDATE.get()};

			if (ITEM._force != true){
				var obj_keys = Object.keys(ITEM._prop);
				for (var i = 0; i < obj_keys.length; ++i) {
					var p = obj_keys[i];
					if (p != "$init" &&  !NODE.LO.isEqual(ITEM._prop[p], ITEM._prop_update[p]) ){
						history_push.from[p] = ITEM._prop[p];
						history_push.to[p] = ITEM._prop_update[p];
					}
				}
			} else {
				history_push.from = JSON.parse(JSON.stringify(ITEM._prop));
				history_push.to = JSON.parse(JSON.stringify(ITEM._prop_update));
			}

			var edited_fields_count = Object.keys(history_push.to).length;

			if (edited_fields_count > 0 && params.schema != "Request_Logs"){
				ITEM._updated = NODE.SDATE.get();
				
				if(!ITEM._dump.skip_history_push){
					ITEM._history.push(history_push);
					ITEM._history_length = ITEM._history.length;
					ITEM.markModified("_history");
				}
				ITEM.markModified("_updated");
				ITEM.markModified("_history_length");
			} 
		

			params.callback()
		})
	} else {	
		params.callback()
	}
}}})()