(function(){'use strict'; module.exports = function(NODE) {return function(params){
	var ITEM = this;

	if (ITEM.isNew == true){
		var runSetupCreate = function(my_uuid){
			if (!ITEM._dump){ITEM._dump = {}};
			if (!ITEM._dump.user){ITEM._dump.user = {"_prop": {}}};

			if (params.schema != "Request_Logs"){
				ITEM._created = NODE.SDATE.get();
				ITEM._updated = NODE.SDATE.get();
				ITEM._history = [{
					by: ITEM._dump.user._prop,
					from: null,
					to: {},
					forced: true,
					date: NODE.SDATE.get(),
				}];
				ITEM._history_length = 1;
			}

			if (!NODE.LO.isNumber(ITEM._clone_generation)){ITEM._clone_generation = 0}

			var last_auto_index = 0;

			NODE.COLS[params.schema].count({}, function(err, total){
				NODE.COLS[params.schema]
				.findOne({"_sequence": {$exists: true}}, "_sequence")
				.sort({_sequence: -1})
				.exec(function(err, last_item){
					if (last_item != undefined && last_item != null){last_auto_index = last_item._sequence}

					NODE.COLS[params.schema]
					.find({"_sequence": {$exists: false}})
					.sort({_created: 1})
					.exec(function(err, items){
						if (!(items != undefined && items != null && items.constructor === Array && items.length == 0)){
							var save_index = 0;
							for (var i = 0; i < items.length; ++i) {saveAutoIndex(items[i])}
						
							function saveAutoIndex(sibling_item){
								sibling_item._sequence = last_auto_index
								last_auto_index++;
								
								sibling_item.markModified("_sequence");
								sibling_item.save(function(err){
									save_index++;
									if (save_index == items.length){
										insertSequence()
									}
								})
							}
						} else {
							last_auto_index++;
							insertSequence()
						}

						function insertSequence(){
							var i,p;
							ITEM._sequence = last_auto_index;

							for (i = 0; i < NODE.SYNC["insert_db_item"].length; ++i) {
								p = NODE.SYNC["insert_db_item"][i];
								if (p.uuid == my_uuid && p.status == "running"){NODE.SYNC["insert_db_item"].splice(i,1); break}
							}

							if (NODE.SYNC["insert_db_item"].length > 0){
								if (NODE.SYNC["insert_db_item"][0].status == "pending"){
									NODE.SYNC["insert_db_item"][0].status = "running";
									NODE.SYNC["insert_db_item"][0].func();
								}
							}

							params.callback()
						}
					})
				})
			})
		}

		if (
			NODE.SYNC["insert_db_item"] == undefined || 
			NODE.SYNC["insert_db_item"] == null
		){
			NODE.SYNC["insert_db_item"] = []
		}; 

		
		if (runSetupCreate != undefined && runSetupCreate != null){
			var uuid = NODE.HELPERS.uuid.generate() 
			NODE.SYNC["insert_db_item"].push({
				uuid: uuid,
				status: "pending",
				func: runSetupCreate.bind(this, uuid)
			});
		}
		
		if (NODE.SYNC["insert_db_item"].length > 0){
			if (NODE.SYNC["insert_db_item"][0].status == "pending"){
				NODE.SYNC["insert_db_item"][0].status = "running";
				NODE.SYNC["insert_db_item"][0].func();
			}
		}
	} else {
		params.callback()
	}
}}})()