(function(){module.exports = function(NODE){
	const glob = require('glob');
	const responses = glob.sync("*app/responses/functions/*").filter(p=>(p.lastChars(8) != ".test.js" && p.firstChars(1) != "_")).map((response)=>{
		var response_file_name = response.split("/")[3];
		var response_type = response_file_name.split(".")[0];
		return {
			type: response_type,
			key: response_file_name.split(".")[1],
		};
	});

	const response_validity_treshold = 1;

	return function(data){
		if (data.type == "message" && data.subtype != "bot_message"){
			if (data.subtype == undefined){data.subtype = "message"}
			var convoMove = function(convo){
				return {
					next: function(func){
						convo._prop.convo_state += 1;
						NODE.COLS.Conversations.update({"_id": convo._id}, {
							"_prop.convo_state" : convo._prop.convo_state
						}, function(err, doc){
							if (NODE.LO.isFunction(func)){func()}
						})
					},
					back: function(func){
						
					},
					set: function(index, func){
						
					},
					end: function(func){
						convo.remove((err)=>{
							if (NODE.LO.isFunction(func)){
								func()
							}
						})
					},
					stash: function(stash_data, func){
						convo._prop.stash = stash_data
						NODE.COLS.Conversations.update({"_id": convo._id}, {
							"_prop.stash" : convo._prop.stash
						}, function(err, doc){
							if (NODE.LO.isFunction(func)){func()}
						})
					}
				}
			}

			var basicMatch = (string_match, response_file)=>{
				var file_name_words = string_match.split(/[ ._|]/).map((filename_word)=>{return filename_word.toLowerCase()});

				var score = 0

				if (!NODE.LO.isEmptyArray(file_name_words) && !NODE.LO.isEmptyArray(response_file.keywords)){
					file_name_words.forEach((filename_word)=>{
						response_file.keywords.forEach((response_word)=>{
							if (
								filename_word.includes(response_word.word) || 
								response_word.word.includes(filename_word)
							){
								score += response_word.score
							}
						})
					})
				}

				return score;
			}

			var processResponse = (user)=>{
				NODE.COLS.Conversations.findOne({"_prop.user_id": String(user._id)}, function(err, convo){
					if (NODE.LO.isNil(convo)){
						var valid_response_types = responses.filter(p=>(data.subtype == p.type)).map((valid_response)=>{
							var file_name = valid_response.type + "." + valid_response.key + ".js";
							var response_file  = require("../responses/functions/" + file_name )(NODE);
							var score = 0;
		
							switch(valid_response.type){
								case "file_share":
									if (!NODE.LO.isEmptyString(data.file.name) && data.file.filetype == "csv" && data.file.mimetype == "text/csv"){
										score = basicMatch(data.file.name, response_file);
									}
									break;
		
								case "message":
									if (!NODE.LO.isEmptyString(data.text)){
										score = basicMatch(data.text, response_file);
									}
									break
								
								default:
									break;
							}
		
							var allowed = false;
		
							if (!NODE.LO.isEmptyArray(response_file.permissions)){
								user._prop.permissions.forEach((permission)=>{
									if (NODE.LO.includes(response_file.permissions, permission)){allowed = true}
								})
							} else {
								allowed = true;
							}
		
							if (allowed == false){score = 0}
		
							return {
								type: valid_response.type,
								key: valid_response.key,
								response_file: response_file,
								file_name: file_name,
								score: score,
							}
						});
		
						var selected_response = null;
		
						if (!NODE.LO.isEmptyArray(valid_response_types)){
							var most_valid_response = NODE.LO.sortBy(valid_response_types, "score").reverse()[0];
							selected_response = (most_valid_response.score >= response_validity_treshold) ? most_valid_response : null;
						}
		
						var response_file = (NODE.LO.isNil(selected_response)) ? require("../responses/functions/_default.js")(NODE) : selected_response.response_file;
						var language_response = (NODE.LO.isNil(selected_response)) ? require("../responses/languages/" + NODE.CONFIG.LANGUAGE + "/_default.js")(NODE) : require("../responses/languages/" + NODE.CONFIG.LANGUAGE + "/" +  selected_response.file_name)(NODE);
	
						if (!NODE.LO.isNil(selected_response)){
							NODE.CONVOSTASH = null;
							convo = new NODE.COLS.Conversations({_prop:{
								user_id: String(user._id),
								convo_file: selected_response.file_name,
								convo_state: 0,
								last_request: NODE.SDATE.get()
							}})

							convo.save((err)=>{
								response_file.func(data, convo, convoMove(convo), language_response);
							})
						} else {
							response_file.func(data, null, convoMove(convo), language_response);
						}
					} else {
						response_file = require("../responses/functions/" + convo._prop.convo_file)(NODE);
						language_response = require("../responses/languages/" + NODE.CONFIG.LANGUAGE + "/" + convo._prop.convo_file)(NODE);
						response_file.func(data, convo, convoMove(convo), language_response)
					}
				})
			}

			NODE.COLS.Users.findOne({"_prop.slack_id": data.user}, (err, user)=>{
				if (NODE.LO.isNil(user)){
					var slack_user = NODE.BOT.getUsers()._value.members.find(p=>(p.id == data.user));
					if (!NODE.LO.isNil(slack_user)){
						var permissions = [];

						var checkPermission = (permission_name)=>{
							if (NODE.LO.includes(NODE.CONFIG.PERMISSIONS[permission_name], slack_user.profile.email) ){
								permissions.push(permission_name)
							}
						}

						checkPermission("godmin");
						checkPermission("admin");

						user = new NODE.COLS.Users({_prop: {
							profile: slack_user.profile,
							slack_id: slack_user.id,
							last_request: new Date("2000-01-01"),
							waiting: false,
							permissions: permissions,
							cluster: "A"
						}})

						user.save((err)=>{
							if (NODE.LO.isNil(err)){
								processResponse(user);
							} else {
								NODE.BOT.postMessage(data.channel, "LMAO, U R NAT MY MASTUR!!!");
							}
						})
					} else {
						NODE.BOT.postMessage(data.channel, "LMAO, U R NAT MY MASTUR!!!");
					}
				} else {
					processResponse(user)
				}
			})
		}
	}
		
}})()