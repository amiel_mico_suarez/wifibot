(function(){
	'use strict';
	var SELF = {};

	module.exports = function(NODE, app){
		SELF = function(){
			this.date_base = new Date();
			this.date_set = new Date();
		};

		SELF.prototype.get = function(year, month, day, hours, minutes, seconds, milliseconds){
			if (year != undefined){
				if (milliseconds != undefined){
					return new Date(year, month, day, hours, minutes, seconds, milliseconds)
				} else if (seconds != undefined){
					return new Date(year, month, day, hours, minutes, seconds)
				} else if (minutes != undefined){
					return new Date(year, month, day, hours, minutes)
				} else if (hours != undefined){
					return new Date(year, month, day, hours)
				} else if (day != undefined){
					return new Date(year, month, day)
				} else if (month != undefined){
					return new Date(year, month)
				} else {
					return new Date(year)
				} 
			} else {
				if(this.date_set != null && this.date_base != null){
					var today = new Date();
					return new Date((new Date(this.date_base).getTime() + today.getTime() - new Date(this.date_set).getTime()))
				} else {
					return new Date();
				}
			}
		};

		return SELF;
	}
		
	
})()