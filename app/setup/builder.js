(function(){
	'use strict';
	var SELF = {};
	var mongoose = require('mongoose');
	var cors = require('cors');
	var moment = require('moment');
	var fs = require('fs');

	module.exports = function(NODE){
		var cors_option = {
			origin: function (origin, callback) {
				if (NODE.config.OAUTH.javascript_origins.indexOf(NODE.config.FRONTEND) !== -1) {
					callback(null, true)
				} else {
					callback(new Error('Not allowed by CORS. Origin is: ' + origin + ". Allowed are: " + JSON.stringify(NODE.config.OAUTH.javascript_origins)))
				}
			}
		}

		SELF.helpers = function(path){
			
			var keys = path.split('/');
			
			var key_name = keys[2].firstChars(keys[2].length - 3);
			if(key_name.charAt(0) != "_"){
				
				NODE.HELPERS[key_name] = require('../helpers/'+key_name+'.js')(NODE);
			}
		};

		// SELF.cron = function(path){
		// 	var keys = path.split('/');
		// 	var key_name = keys[2].firstChars(keys[2].length - 3);
		// 	if(key_name.charAt(0) != "_"){
		// 		NODE.CRON[key_name] = require('../cron/'+key_name+'.js')(NODE);
		// 	}
		// };

		SELF.middlewares = function(path){
			var keys = path.split('/');
			var key_name = keys[2].firstChars(keys[2].length - 3);
			if(key_name.charAt(0) != "_"){
				NODE.MIDDLEWARES[key_name] = require('../middlewares/'+key_name+'.js')(NODE);
			}
		};

		SELF.models = function(path){
			var keys = path.split('/');
			var key_name = keys[2];
			var pre_save_dump = {};
			var isNew = false;
			if(key_name.charAt(0) != "_"){
				NODE.MODELS[key_name] = require('../models/'+key_name+'/model.js');
				try {NODE.MODELS[key_name].preValidate = require('../models/'+key_name+'/preValidate.js');}catch(e){}
				try {NODE.MODELS[key_name].postValidate = require('../models/'+key_name+'/postValidate.js');}catch(e){}
				try {NODE.MODELS[key_name].postSave = require('../models/'+key_name+'/postSave.js');}catch(e){}
				try {NODE.MODELS[key_name].postFetch = require('../models/'+key_name+'/postFetch.js');}catch(e){}
				try {NODE.MODELS[key_name].preDelete = require('../models/'+key_name+'/preDelete.js');}catch(e){}

				NODE.MODELS[key_name].pre("validate", function(next){
					var item = this;
					if (NODE.LO.isNil(item._dump)){item._dump = {}};
					item._dump.post_save_cleaner = false;

					if (item._dump.cleaner == true){
						next();
					} else {
						if (NODE.MODELS[key_name].preValidate != undefined ){
							NODE.MODELS[key_name].preValidate(NODE).bind(item)({schema: key_name, callback: function(pre_validate_error){
								if ( !NODE.LO.isEmptyArray(pre_validate_error)  ){
									var next_error = new Error(JSON.stringify(pre_validate_error));
									next(next_error);
								} else {
									runValidation() 
								}
							}})
						} else {
							runValidation()
						}
					}

					function runValidation(){ 
						NODE.MIDDLEWARES.setupUpdate.bind(item)({schema: key_name, callback: function(setup_err){
							
							if (item._soft_delete == true || item._soft_delete == "true" || Object.keys(NODE.MODELS[key_name].obj._prop).length <= 0){
								if (item._prop_update != undefined && item._prop_update != null){
									item._prop =  JSON.parse(JSON.stringify(item._prop_update))
								}

								goNext(setup_err)
							} else {
								NODE.MIDDLEWARES.validateSave.bind(item)({schema: key_name, callback: function(validate_err){
								NODE.MIDDLEWARES.autoValue.bind(item)({schema: key_name, callback: function(auto_err){
									goNext(validate_err)	
								}})
								}})
							}

							function goNext(validate_err){
								
								if (NODE.MODELS[key_name].postValidate != undefined){
									NODE.MODELS[key_name].postValidate(NODE).bind(item)({schema: key_name, callback: function(post_validate_error){
										
										if ( !NODE.LO.isEmptyArray(post_validate_error)  ){
											for (var i = 0; i < post_validate_error.length; ++i) {validate_err.push(post_validate_error[i])};
										}
										
										defaultNext()
									}})
								} else {
									defaultNext()
								}

								function defaultNext(){
									item._force = undefined;
									item._prop_update = undefined;
									item._soft_delete = undefined;
									isNew = item.isNew;
									if(!NODE.LO.isNil(item._dump)){pre_save_dump = JSON.parse(JSON.stringify(item._dump))}

									item._dump = undefined;

									var next_error = null;
									if ( !NODE.LO.isEmptyArray(validate_err)  ){next_error = new Error(JSON.stringify(validate_err))}

									if (item.isNew){
										item._dump = {};
										item._dump._new = true;
									}
									
									next(next_error);
								}
							}
						}})
					}
				})

				NODE.MODELS[key_name].pre("save", function(next){
					var item = this;
					if (!(!NODE.LO.isNil(item._dump) && item._dump.cleaner == true)){
						NODE.MIDDLEWARES.setupInsert.bind(item)({schema: key_name, callback: function(err){						
							next()
						}})
					} else {
						item._dump.cleaner = false;
						item._dump.post_save_cleaner = true;
						next()
					}
					
				})

				NODE.MODELS[key_name].post("save", function(doc){
					var item = this;

					
					if (!(!NODE.LO.isNil(item._dump) && (item._dump.cleaner == true || item._dump.post_save_cleaner == true))){
						if (NODE.MODELS[key_name].postSave != undefined){
							item._dump = pre_save_dump;
							item.isNew = isNew;
							NODE.MODELS[key_name].postSave(NODE).bind(item)({schema: key_name, callback: function(post_save_error){
								defaultNext()
							}})
						} else {
							defaultNext()
						}


						function defaultNext(){
							
							if (item._dump != undefined && item._dump._new == true){
								
								if (!NODE.LO.isEmptyArray(NODE.SYNC["insert_db_item"])){
									NODE.SYNC["insert_db_item"].splice(0,1);


									if (NODE.SYNC["insert_db_item"].length > 0){

										if (!NODE.LO.isNil(NODE.SYNC["insert_db_item"][0])){
											if (NODE.SYNC["insert_db_item"][0].status == "pending"){
												NODE.SYNC["insert_db_item"][0].status = "running";
												NODE.SYNC["insert_db_item"][0].func();
											}
										} else {
											NODE.SYNC["insert_db_item"].splice(0,1);
											loopChecker();
										}
	
	
										function loopChecker(){
											if (NODE.SYNC["insert_db_item"].length > 0){
												if (!NODE.LO.isNil(NODE.SYNC["insert_db_item"][0])){
													if (NODE.SYNC["insert_db_item"][0].status == "pending"){
														NODE.SYNC["insert_db_item"][0].status = "running";
														NODE.SYNC["insert_db_item"][0].func();
													}
												} else {
													NODE.SYNC["insert_db_item"].splice(0,1);
													loopChecker()
												}
											}
											
										}
										
										
									}
								} 
							}
						}
					}
				})

				NODE.COLS[key_name] = mongoose.model(key_name, NODE.MODELS[key_name]);
			}
		};

		SELF.model_function = function(key_name){
			var model_prop_keys = Object.keys(NODE.MODELS[key_name].obj._prop);
			var model_mimic = []
			for (var i = 0; i < model_prop_keys.length; ++i) {
				var p = model_prop_keys[i];
				var key_params = NODE.MODELS[key_name].obj._prop[p];
				
				if (key_params.func === "mimic" && key_params.collection != undefined){

					
					
					model_mimic.push({
						prefix: p,
						collection: key_params.collection
					})

					delete NODE.MODELS[key_name].obj._prop[p];
				}
			}

			model_mimic.forEach(function(item){
				var model_to_mimic = NODE.MODELS[item.collection].obj._prop;
				var model_to_mimic_keys = Object.keys(model_to_mimic);

				for (var i = 0; i < model_to_mimic_keys.length; ++i) {
					var p = model_to_mimic_keys[i];
					var obj = {};

					var mimic_preffix = item.prefix + "_";
					if(mimic_preffix.firstChars(6) == "_mimic"){mimic_preffix = ""}


					obj["_prop." + mimic_preffix + p] =  model_to_mimic[p]
					

					NODE.MODELS[key_name].add(obj)

					NODE.HELPERS.keyScrapper.set(NODE.MODELS[key_name].obj, "_prop." + item.prefix + "_" + p, model_to_mimic[p])
				}
			})
		}
		
		return SELF;
	}
})()