(function(){
	'use strict';


	module.exports = function(NODE){
		return function(path){
			var fs = require('fs');

			var path_array = path.split("/");

			for (var i = 0; i < path_array.length; ++i) {run(path_array[i], i)}

			function run(path_item, limit){
				var path_string = "";
				var loop_index = 0;

				while (loop_index <= limit){path_string += path_array[loop_index] + '/'; loop_index++ };
				path_string.substr(0, path_string.length - 2);

				if (!fs.existsSync(path_string)){fs.mkdirSync(path_string)};
			}
		}
	}

})()