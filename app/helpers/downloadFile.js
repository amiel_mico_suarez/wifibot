(function(){'use strict'; module.exports = (NODE)=>{return (params)=>{
	var fs = require("fs");
	var https = require('https');

	var destination_folders = params.destination.split("/");
	var destination_directory = destination_folders.splice(0, destination_folders.length - 1).join("/");

	NODE.HELPERS.createDirectory(destination_directory);
	var file = fs.createWriteStream(params.destination);

	var request = https.get(params.options, function(file_response) {
		file_response.pipe(file);
		file.on('finish', function() {file.close(params.callback)});
	})
	
	request.on('error', function(err) {fs.unlink(params.destination); if (params.callback) params.callback(err.message)});
}}})()