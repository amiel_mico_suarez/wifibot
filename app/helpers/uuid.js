(function(){
	'use strict';
	var request = require('request');
	var SELF = {};

	module.exports = function(NODE){
		SELF.generate = function(option){
			var sequence_count = (NODE.LO.isNumber(option)) ? option : 8;
			var sequence_digits = 8;
			var sequence_index = 8;
			var random_digit, random_string = "";

			var date_origin = new Date("2015-01-01").getTime();
			var date_time_current = NODE.SDATE.get().getTime();

			var date_string = Number(date_time_current - date_origin).toString(36);
			date_string = date_string.split("").reverse().join("");
		
			var date_string_index = 0;
			
			while(sequence_count > 0){
				sequence_index = sequence_digits;
				while(sequence_index > 0){
					random_digit = Math.round(Math.random()*35).toString(36);
					random_string += String(random_digit);

					if (Math.random()*100 > 50){
						random_string += date_string.charAt(date_string_index)
						date_string_index ++;
						if (date_string_index > date_string.length - 1){date_string_index = 0}
						sequence_index -- 
					}

					sequence_index -- 
				}
				
				sequence_count--;
				if (sequence_count > 0){random_string += "-"};
			}

			return random_string
		}
	return SELF;
	}

})()