(function(){module.exports = function(NODE){return {
	permissions: ["admin", "godmin"],
	keywords: [
		{word:"vouchers", score: 10},
		{word:"captive", score: 5},
		{word:"portal", score: 1},
	],
	func: (data, convo, move, lang)=>{
		const fs = require("fs");
		const csvParse = require('csv-parse');

		var readUploaded = ()=>{
			var file_name = "./files/keys/" + data.ts + ".csv";
		
			NODE.HELPERS.downloadFile({
				options: {
					"method": "GET",
					"hostname": "files.slack.com",
					"path": data.file.url_private_download,
					"rejectUnauthorized": "false",
					"headers": {
						"Authorization": "Bearer " + NODE.CONFIG.SLACK_TOKEN
					}
				},
				destination: file_name,
				callback: function(err){if (err != undefined){return console.log(err)}
					fs.readFile(file_name, "utf8", function (error, file_content) {
						csvParse(file_content, {comment: '#', auto_parse: true, }, function(err, output){
							var wifi_keys = []
							output.forEach((output_item)=>{
								if (NODE.LO.isArray(output_item)){
									output_item.forEach((output_item_item)=>{
										wifi_keys.push(output_item_item.replace(" ", ""))
									})
								} else {
									wifi_keys.push(output_item.replace(" ", ""))
								}
							})

							var key_number = 0;
							var key_saved = 0;
							var key_saved_ids = [];

							wifi_keys.forEach((wifi_key)=>{
								var new_key =  new NODE.COLS.Wifi_Keys({_prop: {
									key: wifi_key,
									status: "Active",
									used_by: null,
									cluster: "A"
								}})

								new_key.save((err)=>{
									if (NODE.LO.isNil(err)){
										key_saved ++;
										key_saved_ids.push(new_key._id)
									}

									key_number++;
									if (key_number == wifi_keys.length){
										if (key_saved > 0){											
											fs.unlinkSync(file_name);

											if (!NODE.LO.isNil(data.file.initial_comment) && (
												data.file.initial_comment.comment.toUpperCase() == "A" ||
												data.file.initial_comment.comment.toUpperCase() == "B"
											)){
												NODE.COLS.Wifi_Keys.update({"_id": {"$in": key_saved_ids}}, {
													"_prop.cluster" : data.file.initial_comment.comment.toUpperCase()
												}, { multi: true }, function(err, docs){
													NODE.BOT.postMessage(data.channel, lang.success(key_saved));
													move.end();
												})
											} else {
												move.stash({key_saved_ids}, ()=>{
													NODE.BOT.postMessage(data.channel, lang.askCluster(key_saved));
													move.next();
												})
											}
										} else {
											NODE.BOT.postMessage(data.channel, lang.error());
											move.end();
										}
									}
								})
							})
						});                   
					});
				}
			})
		}

		var setCluster = ()=>{
			var key_saved_ids = convo._prop.stash.key_saved_ids;
			if (data.type == "message" && data.subtype == "message" && (
				data.text.toUpperCase() == "A" ||
				data.text.toUpperCase() == "B"
			)){
				NODE.COLS.Wifi_Keys.update({"_id": {"$in": key_saved_ids}}, {
					"_prop.cluster" : data.text.toUpperCase()
				}, { multi: true }, function(err, docs){
					NODE.BOT.postMessage(data.channel, lang.finishValid());
					move.end();
				})
			} else {
				NODE.BOT.postMessage(data.channel, lang.finishInvalid());
				move.end();
			}
		};

		switch(convo._prop.convo_state){
			case 0: move.next(readUploaded); break
			case 2: move.next(setCluster)
		}
	}
}}})()