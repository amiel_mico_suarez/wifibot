(function(){module.exports = function(NODE){return {
	permissions: ["admin", "godmin"],
	keywords: [
		{word:"user", score: 10},
		{word:"employee", score: 10},
		{word:"cluster", score: 5},
	],
	func: (data, convo, move, lang)=>{
		const fs = require("fs");
		const csvParse = require('csv-parse');

		var createUser = (found_user)=>{
			var permissions = [];

			var checkPermission = (permission_name)=>{
				if (NODE.LO.includes(NODE.CONFIG.PERMISSIONS[permission_name], found_user.slack_user.profile.email) ){
					permissions.push(permission_name)
				}
			}

			checkPermission("godmin");
			checkPermission("admin");

			var new_user = new NODE.COLS.Users({_prop: {
				profile: found_user.slack_user.profile,
				slack_id: found_user.slack_user.id,
				last_request: new Date("2000-01-01"),
				waiting: false,
				permissions: permissions,
				cluster: found_user.cluster
			}})

			new_user.save()
		}

		var readUploaded = ()=>{
			var file_name = "./files/keys/" + data.ts + ".csv";
		
			NODE.HELPERS.downloadFile({
				options: {
					"method": "GET",
					"hostname": "files.slack.com",
					"path": data.file.url_private_download,
					"rejectUnauthorized": "false",
					"headers": {
						"Authorization": "Bearer " + NODE.CONFIG.SLACK_TOKEN
					}
				},
				destination: file_name,
				callback: function(err){if (err != undefined){return console.log(err)}
					fs.readFile(file_name, "utf8", function (error, file_content) {
						csvParse(file_content, {comment: '#', auto_parse: true, }, function(err, output){
							var found_users = [];
							var slack_ids = [];
							output.forEach((output_item)=>{
								if (output_item.length == 2 && (
									output_item[1].toUpperCase() == "A" ||
									output_item[1].toUpperCase() == "B"
								)){
									var slack_user = NODE.BOT.getUsers()._value.members.find((p)=>{
										if (
											p.is_bot != true &&
											(
												p.profile.email == output_item[0] ||
												p.profile.display_name == output_item[0] ||
												p.profile.real_name.toLowerCase() == output_item[0].toLowerCase()
											)
										){
											return true
										} else {
											return false
										}
									});

									if (!NODE.LO.isNil(slack_user)){
										slack_ids.push(slack_user.id)
										found_users.push({
											slack_user,
											cluster: output_item[1].toUpperCase()
										})
									}
								} 
							})

							if (!NODE.LO.isEmptyArray(found_users)){
								NODE.COLS.Users.find({"_prop.slack_id": {"$in": slack_ids}}, (err, users)=>{
									if (!NODE.LO.isEmptyArray(users)){
										users.forEach((user)=>{
											var found_user = found_users.find(p=>(p.slack_user.id == user._prop.slack_id));
											found_users = found_users.filter(p=>(p.slack_user.id != user._prop.slack_id));
											user._prop.cluster = found_user.cluster;
											user.markModified("_prop");
											user.save((err)=>{});
										})

										if (!NODE.LO.isEmptyArray(found_users)){
											found_users.forEach((found_user)=>{createUser(found_user)})
										}
									} else {
										found_users.forEach((found_user)=>{createUser(found_user)})
									}
								})
								NODE.BOT.postMessage(data.channel, lang.success(found_users.length))
								move.end()
							} else {
								NODE.BOT.postMessage(data.channel, lang.invalidFile())
								move.end()	
							}
						});                   
					});
				}
			})
		}

		switch(convo._prop.convo_state){
			case 0: move.next(readUploaded); break
		}
	}
}}})()