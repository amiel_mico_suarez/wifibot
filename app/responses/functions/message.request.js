(function(){module.exports = function(NODE){return {
	permissions: [],
	keywords: [
		{word:"request", score: 10},
		{word:"wifi", score: 10},
		{word:"penge", score: 10},
	],
	func: (data, convo, move, lang)=>{
		var moment = require("moment");
		
		NODE.COLS.Users.findOne({"_id":convo._prop.user_id}, function(err, user){

			var notifyAdmin = (noti_text)=>{
				NODE.COLS.Users.find({"_prop.permissions":"admin"}, "_prop", (err, admin_users)=>{
					admin_users.forEach((admin_user)=>{
						NODE.BOT.postMessage(admin_user._prop.slack_id, noti_text);
					})
				})
			}

			var sendWifiKey = (key)=>{
				var qr = require('qr-image');
				var fs = require('fs');

				var qr_svg = qr.image(key, { type: 'png' });
				qr_svg.pipe(fs.createWriteStream("./files/qr/" + key+'.png'));

				NODE.BOT.postMessage(data.channel, key, {
					text: "",
					attachments: [

						{
							"fallback": key,
							"title": key,
							"title_link": NODE.CONFIG.HOST + ":" + NODE.CONFIG.PORT + "/" +key+".png",
							"image_url": NODE.CONFIG.HOST + ":" + NODE.CONFIG.PORT + "/" +key+".png",
							"color": "#764FA5"
						}
					]
				});
			}

			var giveWifiKey = (param)=>{
				var last_request_date = new Date(moment(user._prop.last_request).format("YYYY-MM-DD"));
				var current_date = new Date(moment(NODE.SDATE.get()).format("YYYY-MM-DD"));
				var date_difference = moment(last_request_date).diff(current_date, "days");
				
				if (date_difference  < 0){
					NODE.COLS.Wifi_Keys.findOne({
						"$and": [
							{"_prop.status":"Active"},
							{"_prop.cluster": user._prop.cluster}
						]
					}, (err, wifi_key)=>{
						if (!NODE.LO.isNil(wifi_key)){
							sendWifiKey(wifi_key._prop.key)

							wifi_key.remove((err)=>{
								user._prop.last_request = NODE.SDATE.get();
								if (NODE.LO.isNil(user._prop.request_dates)){user._prop.request_dates = []};
								user._prop.request_dates.push(NODE.SDATE.get());
								user.markModified("_prop");
								user.save((err)=>{
									NODE.COLS.Wifi_Keys.find({
										"$and": [
											{"_prop.status":"Active"},
											{"_prop.cluster": user._prop.cluster}
										]
									}, "_prop", (err, wifi_keys)=>{
										if (wifi_keys.length < 20){
											notifyAdmin(lang.adminFewKeys(wifi_keys.length, user._prop.cluster))
										}
									})

									move.end();
								})
							})
						} else {
							NODE.BOT.postMessage(data.channel, lang.emptyKeys());
							user._prop.waiting = true;
							user.markModified("_prop");
							user.save((err)=>{
								notifyAdmin(lang.adminEmptyKeys(user._prop.cluster))
								move.end();
							})
						}
					})
				} else {
					NODE.BOT.postMessage(data.channel, lang.notAllowed());
					move.end();
				}
			}
	
			switch(convo._prop.convo_state){
				case 0: move.next(giveWifiKey); break
			}
		})
	}
}}})()