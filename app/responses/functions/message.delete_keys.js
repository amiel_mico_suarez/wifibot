(function(){module.exports = function(NODE){return {
	permissions: ["admin", "godmin"],
	keywords: [
		{word:"delete_keys", score: 10},
		{word:"clear_keys", score: 10},
	],
	func: (data, convo, move, lang)=>{
		var moment = require("moment");
		var mongoose = require("mongoose")
		mongoose.connection.db.dropCollection("wifi_keys");

		NODE.BOT.postMessage(data.channel, lang.success());
		move.end();
	}
}}})()