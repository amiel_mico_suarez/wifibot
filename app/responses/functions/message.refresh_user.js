(function(){module.exports = function(NODE){return {
	permissions: ["admin", "godmin"],
	keywords: [
		{word:"refresh", score: 10},
		{word:"erase", score: 10},
	],
	func: (data, convo, move, lang)=>{
		var refreshUser = ()=>{
			var emails_to_refresh = [];
			var email_pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
			data.text.split(/[ ,]/).forEach((data_text)=>{
				var data_array = data_text.split("|");
				data_array.forEach((data_item)=>{
					data_item = data_item.replace("<", "").replace(">", "").replace("mailto:", "");

					var is_email = email_pattern.test(data_item);
					if (is_email == true){emails_to_refresh.push(data_item)}
				})
			})

			var ids_to_refresh = [];
			data.text.split(/[ ,]/).forEach((data_text)=>{
				data_text = data_text.replace("<@", "").replace(">", "")
				ids_to_refresh.push(data_text)
			})

			var select_all = false;
			var all_keywords = ["all", "lahat"]
			data.text.split(/[ ,]/).forEach((data_text)=>{
				if (all_keywords.includes(data_text.toLowerCase())){
					select_all = true;
				}
			})

			if (select_all == true || !NODE.LO.isEmptyArray(emails_to_refresh) || !NODE.LO.isEmptyArray(ids_to_refresh)){
				var user_filter = select_all ? {} : {"$or": [
					{"_prop.profile.email":  { "$in": emails_to_refresh } },
					{"_prop.slack_id": { "$in": ids_to_refresh }}
				]};

				NODE.COLS.Users.find(user_filter, (err, users)=>{
					if (!NODE.LO.isEmptyArray(users)){
						var refresh_count = 0;
						users.forEach((user_refresh)=>{
							user_refresh._prop.last_request = new Date("2000-01-01");
							user_refresh.markModified("_prop.last_request");
							user_refresh.save((err)=>{
								refresh_count++;
								if (refresh_count == users.length){
									NODE.BOT.postMessage(data.channel, lang.success(refresh_count));
									move.end();
								}
							})
						})
					} else {
						NODE.BOT.postMessage(data.channel, lang.error());
						move.end();
					}
				})
			} else {
				NODE.BOT.postMessage(data.channel, lang.error());
				move.end();
			}
		}

		switch(convo._prop.convo_state){
			case 0: move.next(refreshUser); break
		}
	}
}}})()