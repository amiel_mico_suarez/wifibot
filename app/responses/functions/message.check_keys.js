(function(){module.exports = function(NODE){return {
	permissions: ["admin", "godmin"],
	keywords: [
		{word:"check", score: 10},
		{word:"count", score: 10},
		{word:"number", score: 10},
	],
	func: (data, convo, move, lang)=>{
		var countKeys = ()=>{
			NODE.COLS.Wifi_Keys.find({}, (err, wifi_keys)=>{
				NODE.BOT.postMessage(data.channel, lang.success(wifi_keys.length));
				move.end();
			})
		}

		switch(convo._prop.convo_state){
			case 0: move.next(countKeys); break
		}
	}
}}})()