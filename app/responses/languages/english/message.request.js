(function(){module.exports = function(NODE){return {
	notAllowed: ()=>{
		return "Employees can only request 1 wifi key per day.  Kindly ask for IT to refresh your request token if you really need another key."
	},

	emptyKeys: ()=>{
		return "Sorry but we ran out of keys to give.  Needless to say, IT has been notified."
	},

	adminEmptyKeys: (cluster)=>{
		return "We're out of cluster " + cluster + " keys and people are already asking."
	},

	adminFewKeys: (count, cluster)=>{
		return "We're down to the last "+ count + " key(s) for cluster " + cluster + "."
	}
	
}}})()