(function(){module.exports = function(NODE){return {
	success: (key_count)=>{
		return key_count + " new key(s) saved!"
	},

	askCluster: (key_count)=>{
		return key_count + " new key(s) saved! However, you didn't specify which cluster these keys belong to.  Please tell me *(A/B)*."
	},

	finishValid: ()=>{
		return "Thanks. You can specify the cluster next time by typing it as a comment when uploading the csv."
	},

	finishInvalid: ()=>{
		return "Yeah. I didn't understand where you wanted to put them, so I'm defaulting to \"A\". You can specify the cluster next time by typing it as a comment when uploading the csv."
	},

	error: ()=>{
		return "No new keys saved."
	}
}}})()