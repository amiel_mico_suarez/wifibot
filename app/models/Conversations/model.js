(function(){
	'use strict';

		var mongoose = require("mongoose");		
		var Schema = mongoose.Schema;
		var schema_items = require("../_default.js")();

		schema_items._prop = {
			user_id: {
				type: Object,
				required: true,
				custom: {
					input_type: "hidden",
				}
			},

			convo_file: {
				type: String,
				custom: {
					input_type: "hidden",
				}
			},

			convo_state: {
				type: Number,
				custom: {
					input_type: "hidden",
				}
			},

			last_request: {
				type: Date,
				required: true,
				custom: {
					input_type: "hidden",
				}
			},

			stash: {
				type: Object,
				custom: {
					input_type: "hidden",
				}
			}
		}

		var Users = new Schema(schema_items);
		module.exports = Users;

})()