(function(){
	'use strict';

		var mongoose = require("mongoose");		
		var Schema = mongoose.Schema;
		var schema_items = require("../_default.js")();

		schema_items._prop = {
			key: {
				type: String,
				required: true,
				unique: true,
				custom: {
					input_type: "hidden",
				}
			},

			status: {
				type: String,
				required: true,
				custom: {
					input_type: "hidden",
					values: ["Active", "Inactive"]
				}
			},

			used_by: {
				type: String,
				ref: "Users",
				custom: {
					input_type: "hidden",
				}
			},
 
			cluster: {
				type: String,
				required: true,
				custom: {
					input_type: "hidden",
					values: ["A", "B"]
				}
			},
		}

		var Wifi_Keys = new Schema(schema_items);
		module.exports = Wifi_Keys;
})()