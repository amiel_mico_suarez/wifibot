(function(){
	'use strict';

	/*
		_created: Date Created. Auto value. Never changing value.
		_updated: Date everytime the items is updated
		_history: Object Filled everytime the item is updated
		_sequence: Number Acts like unique ID
		_cache: Object but the values are optional and NOT validated
		_dump: Free Object
		_prop: Object where actual fields are being saved
	*/


	/*	-------------------------------------------------------------------------------------------------
		| field_name: {
		|	---------------------------------------------------------------------------------------------
		|	| type: 	Datatype			
		|	| ref:		String name of other collection							
		|	| required: True/False (mongoose ready-made functionalities)
		|	| unique: 	True/False (mongoose ready-made functionalities)	
		|	| custom: 	Free Object {
		|	|		--------------------------------------------------------------------------------------
		|	|  		| input_type: 			String. custom, text, number, textarea, 
		|	|  		|			  			date etc. once you indicate "custom" you
		|	|  		|			  			have to supply "template"
		|	|  		| template: 			String. Optional but Required if input_type="custom"
		|	|		| length: 				Number to validate size of inputted data
		|	|		| hidden: 				True/False use to hide field in frontend
		|	|		| foreign: 				True/False if True, therefore the collection is inside the system
		|	|		| foreign_external: 	True/False if True, therefore the collection is outside the system
		|	|		| foreign_display: 		String that contains fieldnames separated by spaces
		|	|		|						Use for dynamically changing display when rendered in list_00
		|	|		| foreign_ref: 			Use for dynamically changing display when rendered for input
		|	|		| step/group/side: 		Use only for fontend template data presentation
		|	|		| server: 				String Please refer to config.EXTERNAL
		|	|		| route: 				String contains route of the external API
		|	|		| action: 				String contains action of the route (required by Helium API)
		|	|		| foreign_collection: 	String refference for external collection name
		|	|		| foreign_filter: 		String refferece for external validation
		|	|		| soft_delete: 			Array contains value that use to compare from field actual value
		|	|		|						If value defined matches the actual value, The system will perform
		|	|		|						Soft Delete validations
		|	|		|
		|	|		| data_ref: 			Depricated but usable when collection is outside the system "_id" by default
		|	|		|
		|	|		| auto_value: {
		|	|		|		------------------------------------------------------------------------------
		|	|		|		| type: 				String "increment" or "pattern" or "foreign_copy"
		|	|		|		| base: 				Number if "increment" (base value like 1 or 0) String if "pattern" ("RT-$test$-$_sequence$")
		|	|		|		|						use wildcard $example$ to define the custom availabity of the keyword
		|	|		|		| default_increment: 	Number use to add value to the base
		|	|		|		|
		|	|		|		| increment_type: 		String the cause the system to catch the "largets", "smallest", or the total increments
		|	|		|		| increment_obj: 		Object contains fieldname and a custom number for specific increment ({name: 0.5})
		|	|		|		| foreign_copy_source: 	String that keeps the adjacent field name where the system will copy its value
		|	|		|		| foreign_copy_display: String contains field name of the desired value of the foreign_copy_source
		|	|		|		|
		|	|		|		-----------------------------------------------------------------------------
		|	|		| }
		|	|		-------------------------------------------------------------------------------------	
		|	|  }							
		|	----------------------------------------------------------------------------------------------
		|
		| }
		--------------------------------------------------------------------------------------------------
	*/


	
	module.exports = function(){
		return {
			_created: Date,
			_updated: Date,
			_locked: Object,
			_locking_limit: Number,
			_history: Array,
			_history_length: Number,
			_sequence: Number,
			_clone_generation: Number,
			_duplicate_sequence: Number,
			_prop_update: Object,
			_force: Boolean,
			_soft_delete: String,
			_dump: {},
			_cache: {},
			_prop: {},
		};
	}

})()