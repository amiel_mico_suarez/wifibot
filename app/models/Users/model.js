(function(){
	'use strict';

		var mongoose = require("mongoose");		
		var Schema = mongoose.Schema;
		var schema_items = require("../_default.js")();

		schema_items._prop = {
			profile: {
				type: Object,
				required: true,
				custom: {
					input_type: "hidden",
				}
			},

			slack_id: {
				type: String,
				required: true,
				custom: {
					input_type: "hidden",
				}
			},

			last_request: {
				type: Date,
				required: true,
				custom: {
					input_type: "hidden",
				}
			},

			permissions: {
				type: Array,
				custom: {
					input_type: "hidden",
					values: [
						"godmin",
						"admin"
					]
				}
			},

			waiting: {
				type: Boolean,
				required: true,
				custom: {
					input_type: "hidden",
				}
			},

			cluster: {
				type: String,
				required: true,
				custom: {
					input_type: "hidden",
					values: ["A", "B"]
				}
			},

			request_dates: {
				type: Array,
				custom: {
					input_type: "hidden",
				}
			}
		}

		var Users = new Schema(schema_items);
		module.exports = Users;

})()