var self = function(){};
var NODE = new self();

process.env.NODE_ENV = process.env.NODE_ENV || 'develop';
// require('./lib/array_prototype.js');

var external_env = require('./config.js');

var config = {
	env: external_env,
    req_pref:'api/',
}

NODE.conf = config;

var express = require('express');
var http = require('http');
var moment = require('moment');
var app = express();
var http = require("http").Server(app);

var slack = require('slack-node');
NODE.slack = new slack(config.env.SLACK_TOKEN);

NODE.app = app;
NODE.chatScript = {
	process: null,
}

NODE.last_slack_log_timestamp = null;

NODE.slackTheDev = function(message){
	var timestamp_template = "*HEY NETRON ADMIN!* \n_" + NODE.getCurrentTime() + "_ \n ";
	
	var timestamp_msg = "";
	var current_time = moment(new Date());
	if (NODE.last_slack_log_timestamp == null){
		NODE.last_slack_log_timestamp = current_time;
		timestamp_msg = timestamp_template
	} else {
		if (Math.abs(current_time.diff(NODE.last_slack_log_timestamp, "seconds")) >= 10){timestamp_msg = timestamp_template}
		NODE.last_slack_log_timestamp = current_time;
	}

	
	NODE.slack.api('chat.postMessage', {
		text: timestamp_msg + message,	
		as_user: true,
		channel:'@amiel.suarez'
	}, function(err, response){
		if (err != null){console.log(err)}
	});
}

NODE.getCurrentTime = function(){
	var log_date = moment(new Date());
	return log_date.format("YYYY/MM/DD - hh:mm:ss a")
}

NODE.runScript = function(scriptPath, callback) {
	if (NODE.chatScript.process == null){
		var childProcess = require('child_process');
		NODE.chatScript.process = childProcess.fork(scriptPath);
		NODE.slackTheDev("NETRON START v1");

		NODE.chatScript.process.on('error', function (err) {
			NODE.slackTheDev('NETRON ERROR: ' + String(err));
		});
	
		NODE.chatScript.process.on('exit', function (code) {
			var message = 'NETRON EXIT: ' + String(code)
			if (code == null || code == 0){message = 'NETRON WAS DISCONNECTED'};
			NODE.slackTheDev(message);
		});

		NODE.chatScript.process.on('message', function (msg) {
			NODE.slackTheDev((msg.log));
		});

		NODE.chatScript.process.on('disconnect', function (code) {NODE.chatScript.process = null});
	} else {
		NODE.slackTheDev("SERVER IS ATTEMPTING TO START MULTIPLE INSTANCES OF NETRON");
	}
}


console.log(NODE.getCurrentTime() +  " ---  server running at port" + config.env.restart_port);	
NODE.runScript('./bot.js', function (err) {});	
var CronJob = require('cron').CronJob;
new CronJob('00 00 04 * * *', function() {
	NODE.slackTheDev("INITIALIZING DAILY NETRON RESTART...");
	setTimeout(function(){
		if (NODE.chatScript.process != null){NODE.chatScript.process.kill()}
		setTimeout(function(){
			NODE.runScript('./bot.js', function (err) {});
		},5000)
	},2000)
}, null, true);

module.exports = NODE.app;