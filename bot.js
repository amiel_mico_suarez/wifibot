require('./lib/prototypes/array.js');
require('./lib/prototypes/string.js');
require('./lib/prototypes/number.js');
require('./lib/prototypes/object.js');

const glob = require('glob');
const fs = require('fs');
const SlackBot = require('slackbots');
const express = require("express");
const app = express();
const http = require('http');

var NODE = {
    HELPERS: {},
    MODELS: {},
    MIDDLEWARES: {},
    COLS: {},
    SYNC: {},
    LO: require('./lib/lodash/import.js')(),
    CONFIG: require('./config.js'),
}

const responseProcessor = require("./app/setup/responseProcessor")(NODE);
const cronRefresh = require('./app/setup/sdate.js')(NODE);
const builder = require("./app/setup/builder")(NODE);
const sdate = require('./app/setup/sdate.js')(NODE);

NODE.SDATE = new sdate();
NODE.BOT = new SlackBot({
    token: NODE.CONFIG.SLACK_TOKEN, 
    name: 'My Bot'
})

const mongoose = require('mongoose');
mongoose.connect("mongodb://"+NODE.CONFIG.DB.USERNAME+":"+NODE.CONFIG.DB.PASSWORD+"@"+NODE.CONFIG.DB.HOST+":"+NODE.CONFIG.DB.PORT+"/"+NODE.CONFIG.DB.DB_NAME);

mongoose.connection.on('open', function(){
    var models = glob.sync("*app/models/*").filter(p=>(p.lastChars(8) != ".test.js"));
    var middlewares = glob.sync("*app/middlewares/*").filter(p=>(p.lastChars(8) != ".test.js"));
    var helpers = glob.sync("*app/helpers/*").filter(p=>(p.lastChars(8) != ".test.js"));

    middlewares.forEach((middleware)=>{builder.middlewares(middleware)});
    models.forEach((model)=>{builder.models(model)});
    helpers.forEach((helper)=>{builder.helpers(helper)});

    NODE.COLS.Users.find({}, (err, users)=>{
        if (!NODE.LO.isEmptyArray(users)){
            users.forEach((user)=>{
                user._prop.conversation = null;
                user.markModified("_prop.conversation");
                user.save((err)=>{})
            })
        }
    })

    NODE.HELPERS.createDirectory('files/qr/');
    app.use(express.static('files/qr/'))

    var server = http.createServer(app);
    server.listen(NODE.CONFIG.PORT, function(){
        NODE.BOT.on('message', responseProcessor);
    
        NODE.BOT.on('start', function() {
            console.log("bot online")
        });
    })
})

mongoose.connection.on('error',function (err) {  
    console.log('Mongoose default connection error: ' + err);
}); 

mongoose.connection.on('disconnected', function () {  
    console.log('Mongoose default connection disconnected'); 
});