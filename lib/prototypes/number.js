Number.prototype.getAlphaEquivalent = function(){
	var alpha_list = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];

	var number = this;
	var power = 0;
	var digits = [];

	var power = 0;

	while(Math.pow(26, power) < number){power++};
	while(power > 0){
		power--;
		var base_power = Math.pow(26, power);
		var base_push = Math.floor(number / base_power)
		digits.push(base_push)
		number = number - (base_push*base_power)
	};

	if (digits.length == 0){digits.push(number)}

	var alpha_equivalent = "";
	for (var i = 0; i < digits.length; ++i) {
		var p = digits[i];
		alpha_equivalent += alpha_list[p - 1]
	}
	
	return alpha_equivalent
}

Number.prototype.toMoneyComma = function(include_decimal){
	var number = this;

	var number_str = Number(number).toFixed(2).toString();
	var whole = number_str.split(".")[0];
	var decimal = number_str.split(".")[1];

	if(decimal){decimal = ("." + decimal)}
	if(!include_decimal){decimal = ''}
	return whole.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + decimal;
}

Number.prototype.hasDecimalValue = function(){
	if((this - parseInt(this)) > 0){return true}
	return false;
}

Number.prototype.autoFixed = function(len = 2){
	var orig_num = this;
	if(this.hasDecimalValue()){
		return Number(orig_num.toFixed(len))
	}
	return parseInt(orig_num);
}

Number.prototype.roundUpToNearest = function(place){
	if (!Boolean(place != undefined && place != null && !isNaN(Number(place)))){place = 100};
	var new_value = this;
	if (place != 0){
		new_value = Math.ceil(this / place) * place;
	} 

	return new_value;
}