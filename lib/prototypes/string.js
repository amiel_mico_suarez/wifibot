String.prototype.capitalizeFirstLetter = function() {
	return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();})
}

String.prototype.inString = function(item){var result = false; for (var i = 0; i < item.length; i++) {if(this.indexOf(item[i]) > -1){result = true; break}}; return result}

String.prototype.firstChars = function(limit){return this.substr(0, limit)};
String.prototype.lastChars = function(limit){return this.substr(this.length-limit, this.length-1)};

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};