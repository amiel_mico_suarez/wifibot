

(function(){"use strict"; module.exports = function(NODE){
	var lodash = require('./lodash.js');

	lodash.isNilDeep = (value, string = "")=>{
		var scrap_array = string.split(".");
		var scrap_value = value;
	
		var scrap_index = 0;
		
		while(scrap_index < scrap_array.length){
			if (scrap_value != undefined){
				scrap_value = scrap_value[scrap_array[scrap_index].replace("_dot_",".")];
				scrap_index++
			} else {
				scrap_index += scrap_array.length
			}
		}
		
		return lodash.isNil(scrap_value)
	}

	lodash.isNilChain = (values)=>{
		var return_bool = false;
		values.forEach((value)=>{if (lodash.isNil(value)){return_bool = true}});
		return return_bool;
	}

	lodash.isEmptyString = (value)=>{
		return Boolean(!lodash.isString(value) || (lodash.isString(value) && value == ""));
	}

	lodash.isEmptyArray = (value)=>{
		return Boolean(!lodash.isArray(value) || (lodash.isArray(value) &&  value.length == 0));
	}

	lodash.isZero = (value)=>{
		return Boolean(lodash.isNumber(value) && value == 0)
	}

	lodash.setNotNil = (value, base)=>{
		return Boolean(value != undefined && value != null) ? value : base
	}

	lodash.checkChildren = (base, string)=>{
		if (!lodash.isNil(base)){
			var return_status = true;
			var current = base;
			var tree = string.split(".");
			for (var i = 0; i < tree.length; ++i) {
				var p = tree[i];
				if (!lodash.isNil(current[p])){
					current = current[p]
				} else {
					i += tree.length;
					return_status = false
				}
			};
			return return_status
		} else {
			return false;
		}
	}

	lodash.checkJSON = (string)=>{
		var return_bool, test_json;
		
		try {
			JSON.parse(string);
			return_bool = true;
		} catch(err){
			return_bool = false;
		}

		return return_bool
	}

	return lodash
}})()